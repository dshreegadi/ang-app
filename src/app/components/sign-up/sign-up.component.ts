import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private _dataService: DataService,
    private router: Router
  ) { }

  isLogin: boolean = false;

  ngOnInit(): void {
    this.initForm();

    this._dataService._dataSource$.subscribe((data) => {
      if (data.hasOwnProperty('username')) {
        this.isLogin = true;
        this.setValue(data)
      }
    });
  }

  setValue(data) {
    let formControl = this.form.controls;
    formControl['email'].setValue(data['email']);
    formControl['username'].setValue(data['username']);
    formControl['phone'].setValue(data['phone']);
    formControl['city'].setValue(data['city']);
    formControl['password'].setValue(data['password']);
    formControl['cpassword'].setValue(data['cpassword']);
  }

  initForm() {
    this.form = this.formBuilder.group({
      username: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      cpassword: [null, [Validators.required]],
      phone: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(10)]],
      city: [null, [Validators.required]],
    });
  }

  onSubmit() {
    if (!this.isLogin) {
      const allData = JSON.parse(this._dataService.getAllData());
      const { username, password } = this.form.value;
      if (allData !== null) {
        const found = allData.find(d => d.username == username && d.password == password);
        if (found !== undefined) {
          this.router.navigate(['/list']);
        }
      }
    } else {
      this._dataService.storeData(this.form.value);
      this.isLogin = false;
      this.form.reset();
    }
  }

  getIsDisabled() {
    const formControls = this.form.controls;
    if (!this.isLogin) {
      return (formControls['username'].valid && formControls['password'].valid)
    } else {
      return this.form.valid
    }
  }


}
