import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(
    private _dataService: DataService,
    private router: Router
  ) { }

  data = [
    { username: 'Dinesh', email: 'dshreegadi@gmail.com', phone: '8888888888', city: 'Pune' },
    { username: 'Dinesh', email: 'dshreegadi@gmail.com', phone: '212121212', city: 'Pune' },
    { username: 'Dinesh', email: 'dshreegadi@gmail.com', phone: '43434343', city: 'Pune' }
  ];

  columns = [
    { key: 'username', label: 'UserName', isEdit: true, isDelete: true },
    { key: 'email', label: 'email', isEdit: true, isDelete: true },
    { key: 'phone', label: 'phone', isEdit: true, isDelete: true },
    { key: 'city', label: 'city', isEdit: true, isDelete: true },
    { key: 'actions', label: 'Actions', isEdit: true, isDelete: true }
  ];

  ngOnInit(): void {
    this.data = JSON.parse(this._dataService.getAllData());
  }


  onRowClick(item, flag) {
    if (flag === 'edit') {
      this.router.navigate(['/dashboard']);
      this._dataService.setEditData(item);
    } else {
      const allData = JSON.parse(this._dataService.getAllData());
      let { username, password } = item;
      console.log(username, password);
      let filtered = []
      if (allData !== null) {
        filtered = allData.filter((d) => {
          return d['username'] !== username;
        });
        sessionStorage.clear();
        this._dataService.storeData(filtered)
      }
    }
  }

}
