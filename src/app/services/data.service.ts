import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private dataSource = new BehaviorSubject({});
  _dataSource$ = this.dataSource.asObservable();


  constructor() { }


  setEditData(data) {
    this.dataSource.next(data);
  }

  storeData(data) {
    let storedData = [];
    storedData = JSON.parse(this.getAllData());
    if (storedData === null) {
      storedData = [];
    }
    if (storedData !== null) {
      storedData.unshift(data);;
      sessionStorage.setItem('data', JSON.stringify(storedData));
    }
  }

  getAllData() {
    return sessionStorage.getItem('data');
  }
}
